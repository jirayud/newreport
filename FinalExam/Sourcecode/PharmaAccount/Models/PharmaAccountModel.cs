using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace PharmaAccount.Models
{
	public class NewsUser : IdentityUser{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Rank { get; set; }
	}
public class NewsPharma{
public int NewsPharmaID { get; set; }
public string Name { get; set; }
public int Amount { get; set; }
public string Status { get; set; }

public string NewsUserId { get; set; }
public NewsUser postUser { get; set; }
}
}