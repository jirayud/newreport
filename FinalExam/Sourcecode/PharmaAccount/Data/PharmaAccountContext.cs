using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using PharmaAccount.Models;
using Microsoft.EntityFrameworkCore;

namespace PharmaAccount.Data
{
public class PharmaAccountContext : IdentityDbContext<NewsUser>
{
public DbSet<NewsPharma> NewsPharma { get; set; }

protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
{
optionBuilder.UseSqlite(@"Data source=PharmaAccount.db");
}
}
}