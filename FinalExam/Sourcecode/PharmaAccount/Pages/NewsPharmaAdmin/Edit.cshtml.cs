using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PharmaAccount.Data;
using PharmaAccount.Models;

namespace PharmaAccount.Pages.NewsPharmaAdmin
{
    public class EditModel : PageModel
    {
        private readonly PharmaAccount.Data.PharmaAccountContext _context;

        public EditModel(PharmaAccount.Data.PharmaAccountContext context)
        {
            _context = context;
        }

        [BindProperty]
        public NewsPharma NewsPharma { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsPharma = await _context.NewsPharma.FirstOrDefaultAsync(m => m.NewsPharmaID == id);

            if (NewsPharma == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(NewsPharma).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NewsPharmaExists(NewsPharma.NewsPharmaID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool NewsPharmaExists(int id)
        {
            return _context.NewsPharma.Any(e => e.NewsPharmaID == id);
        }
    }
}
