using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PharmaAccount.Data;
using PharmaAccount.Models;

namespace PharmaAccount.Pages.NewsPharmaAdmin
{
    public class IndexModel : PageModel
    {
        private readonly PharmaAccount.Data.PharmaAccountContext _context;

        public IndexModel(PharmaAccount.Data.PharmaAccountContext context)
        {
            _context = context;
        }

        public IList<NewsPharma> NewsPharma { get;set; }

        public async Task OnGetAsync()
        {
            NewsPharma = await _context.NewsPharma.ToListAsync();
        }
    }
}
