using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using PharmaAccount.Data;
using PharmaAccount.Models;

namespace PharmaAccount.Pages.NewsPharmaAdmin
{
    public class CreateModel : PageModel
    {
        private readonly PharmaAccount.Data.PharmaAccountContext _context;

        public CreateModel(PharmaAccount.Data.PharmaAccountContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public NewsPharma NewsPharma { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.NewsPharma.Add(NewsPharma);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}