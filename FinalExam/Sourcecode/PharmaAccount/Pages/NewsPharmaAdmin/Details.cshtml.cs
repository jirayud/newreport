using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PharmaAccount.Data;
using PharmaAccount.Models;

namespace PharmaAccount.Pages.NewsPharmaAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly PharmaAccount.Data.PharmaAccountContext _context;

        public DetailsModel(PharmaAccount.Data.PharmaAccountContext context)
        {
            _context = context;
        }

        public NewsPharma NewsPharma { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsPharma = await _context.NewsPharma.FirstOrDefaultAsync(m => m.NewsPharmaID == id);

            if (NewsPharma == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
